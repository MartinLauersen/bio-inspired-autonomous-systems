clc; close; clear all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                Lizard ear model parameters                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sample_rate = 48000; % sound sampling frequency

ear_separation = 13; % diatance between "ears" in mm
sf = ear_separation/13; % scaling factor to scale the ears up or down in size

% Convert analog Ipsilateral (I) and Contralateral (C) filters to
% digital I and C filters at 48 KHz sampling rate
[bz_I,az_I]=impinvar([-62312.8*sf , 0*sf] , ...
                     [1*sf , 8769.08*sf , 4.26601e8*sf , 1.78616e12*sf , 2.91404e16*sf] , ...
                     sample_rate*sf);
[bz_C,az_C]=impinvar([0.000560749*sf , 2.45862*sf , 114218*sf , 0*sf] , ...
                     [1*sf , 8769.08*sf , 4.26601e8*sf , 1.78616e12*sf , 2.91404e16*sf] , ...
                     sample_rate*sf);
                 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                   Simulation parameters                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

start_freq = 100; % minimum sound frequency 
end_freq = 3000; % maximum sound frequency 
freq_steps = 5; % sound frequency step size

start_incidence_angle = -pi; % maximum sound direction to the right
end_incidence_angle = pi; % maximum sound direction to the left
angle_steps = pi/36; % sound direction step size

n_samples = 500; % number of sound data samples to use

t = 0:1/sample_rate:n_samples/sample_rate; % time base to generate sound signal
fs = start_freq:freq_steps:end_freq;
thetas = start_incidence_angle:angle_steps:end_incidence_angle;

phase_shift_L = 0.0; % phase shift in the left signal (set to 0 while relative
                     % phase shift in the right signal is calculated)
                     
result = zeros((end_freq-start_freq)/freq_steps, (end_incidence_angle-start_incidence_angle)/angle_steps);
                     
%%%%%%%%%%%%%%%%%%%%%%%%%%% Add your code here %%%%%%%%%%%%%%%%%%%%%%%%%%%%
for f = fs
    for theta = thetas
        % a) Phase shift
        phase_shift_R = 2 * pi * f * ear_separation * sin(theta) / 340000;
        
        % b) Sine wave sounds
        sine_L = sin(2 * pi * f * t + phase_shift_L);
        sine_R = sin(2 * pi * f * t + phase_shift_R);
        
        % c) Filtered sine waves
        CL = filter(bz_C, az_C, sine_L);
        IL = filter(bz_I, az_I, sine_L);
        CR = filter(bz_C, az_C, sine_R);
        IR = filter(bz_I, az_I, sine_R);
        
        % d) Ear model outputs
        outL = 20 * log10(sum(abs(IL + CR)));
        outR = 20 * log10(sum(abs(IR + CL)));
        
        row = fix((f-start_freq)/freq_steps + 1);
        col = fix((theta-start_incidence_angle)/angle_steps + 1);
        
        % e) Model response
        result(row, col) = outL - outR;
    end
end
%% Plotting
contourf(rad2deg(thetas), fs / 1e3, result, 'levels', 4);
ylabel("Frequency (kHz)");
xlabel("Sound direction (deg)");
colormap(hot);
c = colorbar("SouthOutside");
c.Label.String = "Vibration amplitude difference (dB)";
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
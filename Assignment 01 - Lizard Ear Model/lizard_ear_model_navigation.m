
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                  User editable parameters                 %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
freqs = [1500, 1700, 2000];
freq = 1000; % sound source frequency in Hz
v_high = 5; % velocity of the faster wheel
v_low = 1;  % velocity of the slower wheel

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%               Lizard ear model parameters                 %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ear_separation = 13; % Distance between "ears" in mm

sf = ear_separation/13;

sample_rate = 48000;

% convert Lei's analog I and C filters to digital I and C filters at 50 KHz sampling rate
[bz_I,az_I]=impinvar([-62312.8,0].*sf,...
                     [1,8769.08,4.26601e8,1.78616e12,2.91404e16].*sf,...
                     sample_rate);
[bz_C,az_C]=impinvar([0.000560749,2.45862,114218,0].*sf,...
                     [1,8769.08,4.26601e8,1.78616e12,2.91404e16].*sf,...
                     sample_rate);

n_samples = 500;
n_op_samples = 50;

t = 0:1/sample_rate:n_samples/sample_rate;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                Misc. simulation parameters                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vL = 0; % cm/s
vR = 0; % cm/s
l = 16; % cm
spkr_xy = [300 300];
robot_pose = [-100; -100; 0];
robot_omega = (vR - vL)/l;

omega = 2*pi*freq;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                    Create plot windows                    %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

scrsz = get(groot,'ScreenSize');
fh = figure('OuterPosition', ...
       [scrsz(1) scrsz(2) scrsz(3) scrsz(4)],...
       'WindowState','maximized'); % [left bottom width height]
% fh = figure()
h2 = subplot(2,2,2);
h2.XLimMode = 'manual';
h2.YLimMode = 'auto';
h2.XLim = [1 100];
hold on;
grid on;
title 'Motor velocities: left motor (red), right motor (black)';

h3 = subplot(2,2,4);
h3.XLimMode = 'manual';
h3.YLimMode = 'auto';
h3.XLim = [1 100];
hold on;
grid on;
title 'Sound inputs: left microphone (red), right microphone (black)';

h1 = subplot(2,2,[1 3]);
h1.XLimMode = 'manual';
h1.YLimMode = 'manual';
h1.XLim = [-200 400];
h1.YLim = [-200 400];
hold on;
axis square;
grid minor;
% title 'Robot arena';

plot(spkr_xy(1),spkr_xy(2),'sk','MarkerFaceColor','k','MarkerSize',12);
robot = plot(robot_pose(1),robot_pose(2),'or','MarkerFaceColor','r',...
             'MarkerSize',10);

dist = 10000000;
v = [vL vR];
vplot1 = plot(h2,v(:,1),'r','LineWidth',2);
vplot2 = plot(h2,v(:,2),'k','LineWidth',2);

o = [0 0];

splot1 = plot(h3,o(:,1),'r','LineWidth',2);
splot2 = plot(h3,o(:,2),'k','LineWidth',2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                     Simulation start                      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p3 = [];
for ts = 1:1000 % in timesteps
    
    dist = sqrt((spkr_xy(2)-robot_pose(2))^2 + ...
                (spkr_xy(1)-robot_pose(1))^2);

    % Determine speaker position relative to the robot
    spkr_angle = atan2(spkr_xy(2)-robot_pose(2),spkr_xy(1)-robot_pose(1));

    % Generate sound signal at "ears" with direction dependent phase shift
    c = omega*ear_separation/340000;
    phase_diff = c*sin(robot_pose(3)-spkr_angle);
    sinewave_L = sin(omega*t - 0);
    sinewave_R = sin(omega*t - phase_diff);

    % Process sound signal using lizard ear model parameters
    Cfilter_output_L = filter(bz_C,az_C,sinewave_L);
    Ifilter_output_L = filter(bz_I,az_I,sinewave_L);
    Cfilter_output_R = filter(bz_C,az_C,sinewave_R);
    Ifilter_output_R = filter(bz_I,az_I,sinewave_R);

    % Calculate left "ear" vibration amplitude in dB
    outL = 20*log10(sum(abs(Ifilter_output_L(n_samples-n_op_samples+1:end) + ...
                             Cfilter_output_R(1,n_samples-n_op_samples+1:end))));
    % Calculate right "ear" vibration amplitude in dB
    outR = 20*log10(sum(abs(Ifilter_output_R(1,n_samples-n_op_samples+1:end) + ...
                             Cfilter_output_L(1,n_samples-n_op_samples+1:end))));

    o(ts,:) = [robot_pose(3)-spkr_angle phase_diff];

    splot1 = plot(h3,o(:,1),'r','LineWidth',2);
    splot2 = plot(h3,o(:,2),'k','LineWidth',2);

    %%%%%%%%%%%%%%%%%% Add your code here %%%%%%%%%%%%%%%%%%%%%%%%%
    
    if outL > outR
        vL = v_low;
        vR = v_high;
        disp("LEFT");
    elseif outL < outR
        vL = v_high;
        vR = v_low;
        disp("RIGHT");
    else
        vL = v_low;
        vR = v_low;
        disp("SINGULARITY");
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    v(ts,1:2) = [vL vR];

    if (vL ~= vR)
        R = (l/2)*(vR + vL)/(vR - vL);
        robot_omega = (vR - vL)/l;
        icc = [robot_pose(1) - (R*sin(robot_pose(3))), ...
               robot_pose(2) + (R*cos(robot_pose(3)))];
        robot_pose = [cos(robot_omega) -sin(robot_omega) 0;...
                      sin(robot_omega)  cos(robot_omega) 0;...
                      0                 0                1]...
                      *...
                      [R*sin(robot_pose(3));...
                      -R*cos(robot_pose(3));...
                      robot_pose(3)]...
                      +...
                      [icc(1);...
                      icc(2);...
                      robot_omega];
    else
        robot_omega = 0;
        robot_pose = robot_pose + ...
                    [cos(robot_pose(3)/v_l); ...
                     sin(robot_pose(3)/v_l); ...
                     0];
    end

    robot = plot(h1,robot_pose(1),robot_pose(2),'or','MarkerFaceColor',...
                 'r','MarkerSize',10);
    line([robot_pose(1),robot_pose(1)+2*cos(robot_pose(3))],...
        [robot_pose(2),robot_pose(2)+2*sin(robot_pose(3))],...
        'LineWidth',2);
    p3(ts, :) = robot_pose;

    a1 = plot(h1,robot_pose(1),robot_pose(2),'.','MarkerSize',2);

    if (ts > 100)
        h2.XLim = [1 ts];
        h3.XLim = [1 ts];
    end

    vplot1 = plot(h2,v(:,1),'r','LineWidth',2);
    vplot2 = plot(h2,v(:,2),'k','LineWidth',2);

    if (dist < 20)
        break;
    end

    pause(0.001);
    
    delete(vplot1);
    delete(vplot2);
    delete(splot1);
    delete(splot2);
    delete(robot);
end

%% Plotting
%close all;
%plot(plotmepls1(:, 1), plotmepls1(:, 2), "*r")
%hold on
%plot(plotmepls2(:, 1), plotmepls2(:, 2), "ob")
%plot(plotmepls3(:, 1), plotmepls3(:, 2), ".g")
%legend(["1", "2", "3"])
%grid on
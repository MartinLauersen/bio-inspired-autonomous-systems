close all
clear all
clc

fp_1700 = load('forplot_1700.mat');
fp_1900 = load('forplot_1900.mat');
fp_2200 = load('forplot_2200.mat');

figure;
plot(fp_1700.forplot(:,1), fp_1700.forplot(:,2))
hold on
plot(fp_1900.forplot(:,1), fp_1900.forplot(:,2))
plot(fp_2200.forplot(:,1), fp_2200.forplot(:,2))
hold off
set(gca, 'XDir','reverse')
leg = legend(["1700", "1900", "2200"], 'Location', 'best')
xlabel("Distance")
ylabel("Amplitude")
title(leg,'Frequency')
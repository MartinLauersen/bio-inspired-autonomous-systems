import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from matplotlib.animation import FuncAnimation

plt.style.use('seaborn-pastel')

ARENA_SIDE_LENGTH = 10
NUMBER_OF_ROBOTS  = 25
STEPS             = 1800
MAX_SPEED         = 0.05
NEIGHBOURHOOD     = 1.5
SEPARATION_DIST   = 0.3
SEPARATION        = 0.0005
ALIGNMENT         = 0.010
COHESION          = 0.002
LIT_MAX           = 1
LIT_COUP          = 0.2
LIT_T             = 0.5
LIT_THRESH        = 1
np.random.seed(420)
# Positions
x = np.random.uniform(low=0, high=ARENA_SIDE_LENGTH, size=(NUMBER_OF_ROBOTS,))
y = np.random.uniform(low=0, high=ARENA_SIDE_LENGTH, size=(NUMBER_OF_ROBOTS,))

# Velocities
vx = np.random.uniform(low=-MAX_SPEED, high=MAX_SPEED, size=(NUMBER_OF_ROBOTS,))
vy = np.random.uniform(low=-MAX_SPEED, high=MAX_SPEED, size=(NUMBER_OF_ROBOTS,))

lit1 = []
# LITNESS
litness = np.random.uniform(low=0, high=LIT_MAX, size=(NUMBER_OF_ROBOTS,))
# Set up the output (1024 x 768):
fig = plt.figure(figsize=(10.24, 7.68), dpi=100)
ax = plt.axes(xlim=(0, ARENA_SIDE_LENGTH), ylim=(0, ARENA_SIDE_LENGTH))

points, = ax.plot([], [], 'bo', lw=0, )
litpoints, = ax.plot([], [], 'ro', lw=0, )

# Make the environment toroidal 
def wrap(z):    
    return z % ARENA_SIDE_LENGTH

def init():
    points.set_data([], [])
    return points,


def getBestXY(curr_x, curr_y, x, y, neighbourhood):
    ret_x = x
    ret_y = y
    if 0 <= curr_x <= neighbourhood:
        if ARENA_SIDE_LENGTH-neighbourhood <= x <= ARENA_SIDE_LENGTH:
            ret_x -= ARENA_SIDE_LENGTH

    elif ARENA_SIDE_LENGTH-neighbourhood <= curr_x <= ARENA_SIDE_LENGTH:
        if 0 <= x <= neighbourhood:
            ret_x += ARENA_SIDE_LENGTH

    if 0 <= curr_y <= neighbourhood:
        if ARENA_SIDE_LENGTH-neighbourhood <= y <= ARENA_SIDE_LENGTH:
            ret_y -= ARENA_SIDE_LENGTH

    elif ARENA_SIDE_LENGTH-neighbourhood <= curr_y <= ARENA_SIDE_LENGTH:
        if 0 <= y <= neighbourhood:
            ret_y += ARENA_SIDE_LENGTH

    return ret_x, ret_y

def separation_alignment_cohesion():
    global x, y, vx, vy
    for i in range(NUMBER_OF_ROBOTS):
        cur_x = x[i]
        cur_y = y[i]
        vects_x = []
        vects_y = []
        vxs = []
        vys = []
        xs = []
        ys = []
        for ii in range(NUMBER_OF_ROBOTS):
            if i == ii:
                continue
            else:
                xx, yy = getBestXY(cur_x, cur_y, x[ii], y[ii], NEIGHBOURHOOD)
                vec_x = cur_x-xx
                vec_y = cur_y-yy
                dist = np.sqrt(vec_x**2 + vec_y**2)

                if dist <= SEPARATION_DIST:
                    vects_x.append(vec_x/(dist**2))
                    vects_y.append(vec_y/(dist**2))


                if dist <= NEIGHBOURHOOD*1.5:
                    vxs.append(vx[ii])
                    vys.append(vy[ii])

                if dist <= NEIGHBOURHOOD:
                    xs.append(xx)
                    ys.append(yy)

        vects_x_avg = 0 if not len(vects_x) else np.mean(vects_x)
        vects_y_avg = 0 if not len(vects_y) else np.mean(vects_y)
        vxs_avg = 0 if not len(vxs) else np.mean(vxs)
        vys_avg = 0 if not len(vys) else np.mean(vys)
        xs_avg = 0 if not len(xs) else np.mean(xs)-cur_x
        ys_avg = 0 if not len(ys) else np.mean(ys)-cur_y

        vx[i] = np.clip(vx[i] + vects_x_avg*SEPARATION + vxs_avg*ALIGNMENT + xs_avg*COHESION, -MAX_SPEED, MAX_SPEED)
        vy[i] = np.clip(vy[i] + vects_y_avg*SEPARATION + vys_avg*ALIGNMENT + ys_avg*COHESION, -MAX_SPEED, MAX_SPEED)

def firefly():
    global x, y

    for i in range(NUMBER_OF_ROBOTS):
        cur_x = x[i]
        cur_y = y[i]

        count = 0
        for ii in range(NUMBER_OF_ROBOTS):
            xx, yy = getBestXY(cur_x, cur_y, x[ii], y[ii], NEIGHBOURHOOD)
            dist = np.sqrt((cur_x-xx) ** 2 + (cur_y - yy) ** 2)

            if dist <= NEIGHBOURHOOD and litness[ii] >= LIT_THRESH:
                count += 1

        litness[i] += 1/(LIT_T*60) + LIT_COUP * count * litness[i]

    unlit = np.where(litness < LIT_THRESH)
    lit = np.where(litness >= LIT_THRESH-0.05)
    reset = np.where(litness >= LIT_THRESH)
    litness[reset] = 0


    return unlit, lit

def animate(i):
    global x, y, vx, vy
    separation_alignment_cohesion()

    # Below are used to implement random walk
    #vx = np.random.uniform(low=-MAX_SPEED, high=MAX_SPEED, size=(NUMBER_OF_ROBOTS,))
    #vy = np.random.uniform(low=-MAX_SPEED, high=MAX_SPEED, size=(NUMBER_OF_ROBOTS,))
    indexes_unlit, indexes_lit = firefly()
    x = np.array(list(map(wrap, x + vx)))
    y = np.array(list(map(wrap, y + vy)))
    #circles.set_data(x, y)
    points.set_data(x[indexes_unlit], y[indexes_unlit])
    litpoints.set_data(x[indexes_lit], y[indexes_lit])
    print('Step ', i + 1, '/', STEPS, end='\r')
    return points,

anim = FuncAnimation(fig, animate, init_func=init,
                               frames=STEPS, interval=1, blit=True)

writervideo = animation.FFMpegWriter(fps=60)
anim.save("flocking_25_eps_02.mp4", writer=writervideo)